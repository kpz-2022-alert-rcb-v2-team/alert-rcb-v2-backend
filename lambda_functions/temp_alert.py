import boto3
import json
from encoder import Encoder

dynamodbTableName = "AlertTemp"
dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table(dynamodbTableName)

getMethod = "GET"
postMethod = "POST"
patchMethod = "PATCH"
deleteMethod = "DELETE"
tempAlertPath = "/tempalert"
tempAlertsPath = "/tempalerts"


def lambda_handler(event, context):
    httpMethod = event["httpMethod"]
    path = event["path"]
    if httpMethod == getMethod and path == tempAlertPath:
        response = getTempAlert(event["queryStringParameters"]["tempAlertId"])
    elif httpMethod == getMethod and path == tempAlertsPath:
        response = getTempAlerts()
    elif httpMethod == postMethod and path == tempAlertPath:
        response = addTempAlert(json.loads(event["body"]))
    elif httpMethod == patchMethod and path == tempAlertPath:
        requestBody = json.loads(event["body"])
        response = editTempAlert(
            requestBody["tempAlertId"],
            requestBody["updateKey"],
            requestBody["updateValue"],
        )
    elif httpMethod == deleteMethod and path == tempAlertPath:
        requestBody = json.loads(event["body"])
        response = deleteTempAlert(requestBody["tempAlertId"])
    else:
        response = buildResponse(404, "Not Found")

    return response


def getTempAlert(tempAlertId):
    response = table.get_item(Key={"tempAlertId": tempAlertId})
    if "Item" in response:
        return buildResponse(200, response["Item"])
    else:
        return buildResponse(
            404, {"Message": f"TempAlert with id {tempAlertId} does not exist"}
        )


def getTempAlerts():
    response = table.scan()
    result = response["Items"]

    returnBody = {"TempAlerts": result}
    return buildResponse(200, returnBody)


def addTempAlert(requestBody):
    table.put_item(Item=requestBody)
    body = {"Operation": "ADD", "Message": "SUCCESS", "Item": requestBody}
    return buildResponse(200, body)


def editTempAlert(tempAlertId, updateKey, updateValue):
    response = table.update_item(
        Key={"tempAlertId": tempAlertId},
        UpdateExpression=f"set {updateKey} = :value",
        ExpressionAttributeValues={":value": updateValue},
        ReturnValues="UPDATED_NEW",
    )
    body = {"Operation": "UPDATE", "Message": "SUCCESS", "UpdatedAttributes": response}
    return buildResponse(200, body)


def deleteTempAlert(tempAlertId):
    response = table.delete_item(
        Key={"tempAlertId": tempAlertId}, ReturnValues="ALL_OLD"
    )
    body = {"Operation": "DELETE", "Message": "SUCCESS", "DeletedAlert": response}
    return buildResponse(200, body)


def buildResponse(statusCode, body=None):
    response = {
        "statusCode": statusCode,
    }
    if body is not None:
        response["body"] = json.dumps(body, cls=Encoder)
    return response
